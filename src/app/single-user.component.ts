import { Component, OnInit } from '@angular/core';
import { UserService } from './shared/user.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'single-user',
    template: `
    <header-component></header-component>
    <p *ngIf="!user.name" class="loading">Loading...</p>
    <ul *ngIf="user.name">
        <li><strong>User:</strong> {{user.name}}</li>
        <li><strong>Username:</strong> {{user.username}}</li>
        <li><strong>Email:</strong> {{user.email}}</li>
        <li><strong>Website:</strong> {{user.website}}</li>
        <li><a [routerLink]="['/users/']">Back</a></li>
    </ul>
    `,
    styles: [`
        ul {
            list-style-type: none;
            margin: 0 auto;
            width: 40%;
        }

        li {
            padding: 10px;
        }

         strong {
             font-weight: 800;
             margin-right: 5px;
         }

         a {
             color: blue;
             float: right;
         }

         .loading {
             color: red;
             font-size: 30px;
             text-align: center;
         }
    `]
})

export class SingleUserComponent implements OnInit {
    user: string = 'Loading...';

    constructor(private userService: UserService, private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
                let userId = params['userId'];
                this.userService.getUser(userId)
                    .subscribe(res => this.user = res)
            });
     }
}
