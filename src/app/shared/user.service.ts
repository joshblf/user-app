import { Injectable } from '@angular/core';
import { Http, Response }  from '@angular/http';
import 'rxjs/add/operator/map';

import { User } from './user';

@Injectable()
export class UserService {
    private _usersUrl: string = 'https://jsonplaceholder.typicode.com/users';

    constructor(private _http: Http) {}

    public getUsers() {
        return this._http.get(this._usersUrl)
            .map((response: Response) => response.json())
    }

    public getUser(id: number) {
        return this._http.get(this._usersUrl + '/' + id)
            .map((response: Response) => response.json())
    }
}
