import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from './header.component';

import { User } from './shared/user';
import { UserService } from './shared/user.service';

@Component({
    moduleId: module.id,
    selector: 'user-list',
    template: `
        <header-component></header-component>
        <p *ngIf="!users.length" class="loading">Loading...</p>
        <ul *ngFor="let user of users" class="user-list">
            <li>
                {{user.id}}: {{user.name}}
                <a [routerLink]="['/users/', user.id]">View User</a>
            </li>
        </ul>
    `,
    styles: [`
        .user-list {
            list-style-type: none; 
            margin: 0 auto;
            width: 40%;
        }

        li {
            padding: 20px 10px;
        }

        a:visited {
            color: blue;
        }

        a {
            font-size: 14px;
            float: right;
            padding-top: 3px;
        }
    `]
})

export class UserComponent implements OnInit {
    users: User[] = [];
    updatingUser: boolean = false;

    constructor(private _userService: UserService) { }

    ngOnInit() { 
        this._userService.getUsers()
            .subscribe(res => this.users = res);
    }

    updateUser() {
        this.updatingUser = true;
    }

    getUser(id: number) {
        this._userService.getUser(id)
            .subscribe(res => {
                console.dir(res);
            });
    }

    getUsers() {
        this._userService.getUsers()
            .subscribe(res => this.users = res);
    }
}
