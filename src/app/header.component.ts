import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'header-component',
    template: `<header>
                    <h1>{{title}}</h1>
                </header>`
})

export class HeaderComponent {
	public title: string = 'User List App';
}
